import useJsonPost from '../hooks/use-json-post';
import { AUTH_URL } from '../constants';
import { useState } from 'react';

export function AuthForm() {
    let [requestOptions, setRequestOptions] = useState(null);
    let [invokeAuth, setInvokeAuth] = useState(false);

    const [data, loading, error] = useJsonPost(AUTH_URL, null, requestOptions, invokeAuth);

    const handleSubmit = (e) => {
        const { target: {[0]: { value: login }, [1]: {value: password}} } = e;
        setRequestOptions({
            headers: {
                'X-Auth-Password': password,
                'X-Auth-Login': login
            }
        });
        setInvokeAuth(true);
        e.preventDefault();
    };
    const retryAuth = () => setInvokeAuth(false);

    const authForm = <>
        <h4>Форма авторизации</h4>
        <div className="container col-4">
            <form onSubmit={handleSubmit}>
                <div className="form-group">
                    <label htmlFor="login">Введите логин</label>
                    <input type="text" className="form-control" id="login" placeholder="login" autoComplete="off" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="password">Введите пароль</label>
                    <input type="password" className="form-control" id="password" placeholder="password" autoComplete="off" required/>
                </div>
                <button className="btn btn-primary" type="submit">Войти</button>
            </form>
        </div>
    </>;
    return (<div>
        {!invokeAuth && authForm }
        {invokeAuth && data && <div className="alert alert-success">Вы успешно авторизованы</div>}
        {invokeAuth && error && <div className="alert alert-danger">Вы не авторизованы.
          <div><button className='btn btn-light' onClick={retryAuth}>Попробовать еще раз</button></div>
        </div>}
    </div>);
}
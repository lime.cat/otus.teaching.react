import { useState, useEffect } from 'react';
import axios from 'axios';

export default function useJsonPost(url, body, opts, invoke) {
    let [data, setData] = useState(null);
    let [loading, setLoading] = useState(false);
    let [error, setError] = useState(null);

    useEffect(() => {
        if (invoke)
        {
            setLoading(true);
            async function postData() {
                try {
                    const response = await axios.post(url, body, opts);
                    console.log(response);
                    if (response.status === 200 || response.status === 204) {
                        setData(response.data);
                    } else {
                        setError(response.error);
                    }
                }
                catch (err) {
                    setError(err);
                }
                finally {
                    setLoading(false);
                }
            }
            postData();
        } 
    }, [url, body, opts, invoke]);    
    
    return [data, loading, error];
}
